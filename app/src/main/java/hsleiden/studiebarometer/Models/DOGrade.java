package hsleiden.studiebarometer.Models;

import android.content.ContentValues;
import android.database.Cursor;

import java.io.Serializable;
import java.util.ArrayList;

import hsleiden.studiebarometer.Database.DatabaseHelper;
import hsleiden.studiebarometer.Database.DatabaseInfo;

/**
 * Created by Yoxx on 13/04/16.
 */
public class DOGrade implements Runnable, Serializable {
    private int id;
    private int uid;
    private int course_id;
    private double grade;
    private int chance;
    private boolean done = false;

    public DOGrade(int course_id, int uid, double grade){
        this.course_id = course_id;
        this.uid = uid;
        this.grade = grade;
        if (grade >= 5.5){
            this.done = true;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getCourseId() {
        return course_id;
    }

    public void setCourseId(int course_id) {
        this.course_id = course_id;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public int getChance() {
        return chance;
    }

    public void setChance(int chance) {
        this.chance = chance;
    }

    public void addChance() {
        this.chance += 1;
    }

    private void saveAsThread() {
        Thread t = new Thread(this, "save_grade_" + this.uid);
        t.start();
    }

    @Override
    public void run() {
        update();
    }

    /**
     * Saves the object to the database
     */
    public void save() {
        if (this.id == 0) {
            DatabaseHelper db = DatabaseHelper.getInstance();
            ContentValues values = new ContentValues();

            values.put(DatabaseInfo.GradeTable.COURSE_ID, this.course_id);
            values.put(DatabaseInfo.GradeTable.UID, this.uid);
            values.put(DatabaseInfo.GradeTable.GRADE, this.grade);
            values.put(DatabaseInfo.GradeTable.CHANCE, this.chance);
            values.put(DatabaseInfo.GradeTable.DONE, this.done);

            int id = db.insert(DatabaseInfo.GradeTable.TABLE, null, values);
            if (id != -1) {
                this.id = id;
            }
        } else {
            saveAsThread();
        }
    }

    public void update() {
        DatabaseHelper db = DatabaseHelper.getInstance();
        ContentValues values = new ContentValues();

        values.put(DatabaseInfo.GradeTable.COURSE_ID, this.course_id);
        values.put(DatabaseInfo.GradeTable.UID, this.uid);
        values.put(DatabaseInfo.GradeTable.GRADE, this.grade);
        values.put(DatabaseInfo.GradeTable.CHANCE, this.chance);
        values.put(DatabaseInfo.GradeTable.DONE, this.done);
        db.update(DatabaseInfo.GradeTable.TABLE, values, "_id = " + this.id);
    }

    /**
     * Deletes the object from DB
     *
     * @return boolean
     */
    public boolean delete() {
        DatabaseHelper db = DatabaseHelper.getInstance();
        return db.remove(DatabaseInfo.GradeTable.TABLE, "_id = " + this.id);
    }

    /**
     * Queries the database and returning a list of objects
     * (if where is null returns all the objects in an ArrayList)
     *
     * @param where String
     * @return ArrayList
     */
    static public ArrayList where(String where) {
        DatabaseHelper db = DatabaseHelper.getInstance();
        ArrayList<DOGrade> list = new ArrayList<DOGrade>();
        Cursor cursor = db.query(DatabaseInfo.GradeTable.TABLE, new String[]{"*"}, where, null, null, null, null);
        while (cursor.moveToNext()) {
            int id = (int) cursor.getInt(cursor.getColumnIndex("_id"));
            int uid = (int) cursor.getInt(cursor.getColumnIndex(DatabaseInfo.GradeTable.UID));
            int course_id = (int) cursor.getInt(cursor.getColumnIndex(DatabaseInfo.GradeTable.COURSE_ID));
            int chance = (int) cursor.getInt(cursor.getColumnIndex(DatabaseInfo.GradeTable.CHANCE));
            double grade = (double) cursor.getDouble(cursor.getColumnIndex(DatabaseInfo.GradeTable.GRADE));

            boolean done;
            if (cursor.getInt(cursor.getColumnIndex(DatabaseInfo.GradeTable.DONE)) == 1) {
                done = true;
            } else {
                done = false;
            }

            DOGrade doGrade = new DOGrade(course_id, uid, grade);
            doGrade.setId(id);
            doGrade.setChance(chance);
            doGrade.setDone(done);

            list.add(doGrade);
        }
        cursor.close();

        return list;
    }

    /**
     * This method returns all the entries in the list.
     *
     * @return ArrayList
     */
    static public ArrayList where() {
        return where(null);
    }

    /**
     * Queries the database and returns a the first object that matches the where
     * (if where is null picks the first object in the database)
     *
     * @param where String
     * @return Module
     */
    static public DOGrade find(String where) {
        DatabaseHelper db = DatabaseHelper.getInstance();
        Cursor cursor = db.query(DatabaseInfo.GradeTable.TABLE, new String[]{"*"}, where, null, null, null, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            int id = (int) cursor.getInt(cursor.getColumnIndex("_id"));
            int uid = (int) cursor.getInt(cursor.getColumnIndex(DatabaseInfo.GradeTable.UID));
            int course_id = (int) cursor.getInt(cursor.getColumnIndex(DatabaseInfo.GradeTable.COURSE_ID));
            int chance = (int) cursor.getInt(cursor.getColumnIndex(DatabaseInfo.GradeTable.CHANCE));
            double grade = (double) cursor.getDouble(cursor.getColumnIndex(DatabaseInfo.GradeTable.GRADE));

            boolean done;
            if (cursor.getInt(cursor.getColumnIndex(DatabaseInfo.GradeTable.DONE)) == 1) {
                done = true;
            } else {
                done = false;
            }

            DOGrade doGrade = new DOGrade(course_id, uid, grade);
            doGrade.setId(id);
            doGrade.setChance(chance);
            doGrade.setDone(done);

            cursor.close();

            return doGrade;
        } else {
            return null;
        }
    }

    @Override
    public String toString(){
        if (grade != 0){
            return "" + this.grade;
        } else if (done) {
            return "gehaald";
        } else{
            return "niet gehaald";
        }
    }
}
