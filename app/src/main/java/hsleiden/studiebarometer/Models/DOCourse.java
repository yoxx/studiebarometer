package hsleiden.studiebarometer.Models;

import android.content.ContentValues;
import android.database.Cursor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import hsleiden.studiebarometer.Database.DatabaseHelper;
import hsleiden.studiebarometer.Database.DatabaseInfo;

/**
 * Created by Yoxx on 13/04/16.
 */
public class DOCourse implements Runnable, Serializable {
    private int id;
    private String name;
    private int ects;
    private int period;
    private boolean core;
    private int uid;

    public DOCourse(String name, int ects, int period){
        this.name = name;
        this.ects = ects;
        this.period = period;
    }

    public int getId(){
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEcts(){
        return this.ects;
    }

    public void setEcts(int ects) {
        this.ects = ects;
    }

    public int getPeriod(){
        return this.period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public void setCore(boolean b){
        this.core = b;
    }

    public boolean isCore(){
        return this.core;
    }

    public String isCoreString() {
        if (isCore()){
            return "Ja";
        } else {
            return "Nee";
        }
    }

    private void saveAsThread() {
        Thread t = new Thread(this, "save_course_" + this.name);
        t.start();
    }

    @Override
    public void run() {
        update();
    }

    /**
     * Saves the object to the database
     */
    public void save() {
        if (this.id == 0) {
            DatabaseHelper db = DatabaseHelper.getInstance();
            ContentValues values = new ContentValues();

            values.put(DatabaseInfo.CourseTable.COURSE_NAME, this.name);
            values.put(DatabaseInfo.CourseTable.ECTS, this.ects);
            values.put(DatabaseInfo.CourseTable.PERIOD, this.period);
            values.put(DatabaseInfo.CourseTable.CORE, this.core);

            int id = db.insert(DatabaseInfo.CourseTable.TABLE, null, values);
            if (id != -1) {
                this.id = id;
            }
        } else {
            saveAsThread();
        }
    }

    public void update() {
        DatabaseHelper db = DatabaseHelper.getInstance();
        ContentValues values = new ContentValues();

        values.put(DatabaseInfo.CourseTable.COURSE_NAME, this.name);
        values.put(DatabaseInfo.CourseTable.ECTS, this.ects);
        values.put(DatabaseInfo.CourseTable.PERIOD, this.period);
        values.put(DatabaseInfo.CourseTable.CORE, this.core);
        db.update(DatabaseInfo.CourseTable.TABLE, values, "_id = " + this.id);
    }

    /**
     * Deletes the object from DB
     *
     * @return boolean
     */
    public boolean delete() {
        DatabaseHelper db = DatabaseHelper.getInstance();
        return db.remove(DatabaseInfo.CourseTable.TABLE, "_id = " + this.id);
    }

    /**
     * Queries the database and returning a list of objects
     * (if where is null returns all the objects in an ArrayList)
     *
     * @param where String
     * @return ArrayList
     */
    static public ArrayList<DOCourse> where(String where) {
        DatabaseHelper db = DatabaseHelper.getInstance();
        ArrayList<DOCourse> list = new ArrayList<DOCourse>();
        Cursor cursor = db.query(DatabaseInfo.CourseTable.TABLE, new String[]{"*"}, where, null, null, null, null);
        while (cursor.moveToNext()) {

            int id = (int) cursor.getInt(cursor.getColumnIndex("_id"));
            String name = (String) cursor.getString(cursor.getColumnIndex(DatabaseInfo.CourseTable.COURSE_NAME));
            int ects = (int) cursor.getInt(cursor.getColumnIndex(DatabaseInfo.CourseTable.ECTS));
            int period = (int) cursor.getInt(cursor.getColumnIndex(DatabaseInfo.CourseTable.PERIOD));

            boolean core;
            if (cursor.getInt(cursor.getColumnIndex(DatabaseInfo.CourseTable.CORE)) == 1) {
                core = true;
            } else {
                core = false;
            }

            DOCourse course = new DOCourse(name, ects, period);
            course.setId(id);
            course.setCore(core);

            list.add(course);
        }
        cursor.close();

        return list;
    }

    /**
     * This method returns all the entries in the list.
     *
     * @return ArrayList
     */
    static public ArrayList where() {
        return where(null);
    }

    /**
     * Queries the database and returns a the first object that matches the where
     * (if where is null picks the first object in the database)
     *
     * @param where String
     * @return Module
     */
    static public DOCourse find(String where) {
        DatabaseHelper db = DatabaseHelper.getInstance();
        Cursor cursor = db.query(DatabaseInfo.CourseTable.TABLE, new String[]{"*"}, where, null, null, null, null);

        cursor.moveToFirst();
        int id = (int) cursor.getInt(cursor.getColumnIndex("_id"));
        String name = (String) cursor.getString(cursor.getColumnIndex(DatabaseInfo.CourseTable.COURSE_NAME));
        int ects = (int) cursor.getInt(cursor.getColumnIndex(DatabaseInfo.CourseTable.ECTS));
        int period = (int) cursor.getInt(cursor.getColumnIndex(DatabaseInfo.CourseTable.PERIOD));

        boolean core;
        if (cursor.getInt(cursor.getColumnIndex(DatabaseInfo.CourseTable.CORE)) == 1) {
            core = true;
        } else {
            core = false;
        }

        DOCourse course = new DOCourse(name, ects, period);
        course.setId(id);
        course.setCore(core);

        cursor.close();

        return course;
    }

    public void setUID(int uid) {
        this.uid = uid;
    }

    @Override
    public String toString() {
        String course;
        DOGrade  grade = DOGrade.find("uid = " + this.uid + " AND course_id = " + this.id);
        if(grade != null) {
            course = this.name + ": " + this.ects + " ECTS  -  " + grade.toString();
        } else {
            course = this.name + ": " + this.ects + " ECTS  -  " + "nog niet ingevuld";
        }
        return course;
    }
}
