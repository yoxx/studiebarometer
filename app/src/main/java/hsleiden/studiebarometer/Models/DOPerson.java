package hsleiden.studiebarometer.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import hsleiden.studiebarometer.Database.DatabaseHelper;
import hsleiden.studiebarometer.Database.DatabaseInfo;

/**
 * Created by Yoxx on 13/04/16.
 */
public class DOPerson implements Runnable, Serializable {
    private int id;
    private String first_name;
    private String last_name;
    private String student_number;
    private int ects;
    private int passedEcts;
    private int failedEcts;

    public DOPerson(String fname, String lname, String snumber) {
        this.first_name = fname;
        this.last_name = lname;
        this.student_number = snumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return this.first_name;
    }

    public void setFirstName(String first_name) {
        this.first_name = first_name;
    }

    public String getLastName() {
        return this.last_name;
    }

    public void setLastName(String last_name) {
        this.last_name = last_name;
    }

    public String getName() {
        return this.first_name + " " + this.last_name;
    }

    public void setEcts(int ects){ this.ects = ects; }

    public void addEcts(int ects){ this.ects += ects; }

    public int getEcts(){ return this.ects; }

    public String getStudentNumber() {
        return this.student_number;
    }

    public void setStudentNumber(String student_number) {
        this.student_number = student_number;
    }

    private void saveAsThread() {
        Thread t = new Thread(this, "save_person_" + this.last_name);
        t.start();
    }

    @Override
    public void run() {
        update();
    }

    /**
     * Saves the object to the database
     */
    public void save() {
        if (this.id == 0) {
            DatabaseHelper db = DatabaseHelper.getInstance();
            ContentValues values = new ContentValues();

            values.put(DatabaseInfo.PersonTable.FIRST_NAME, this.first_name);
            values.put(DatabaseInfo.PersonTable.LAST_NAME, this.last_name);
            values.put(DatabaseInfo.PersonTable.STUDENT_NUMBER, this.student_number);

            int id = db.insert(DatabaseInfo.PersonTable.TABLE, null, values);
            if (id != -1) {
                this.id = id;
            }
        } else {
            saveAsThread();
        }
    }

    public void saveWithPassword(String password) {
        if (this.id == 0) {
            DatabaseHelper db = DatabaseHelper.getInstance();
            ContentValues values = new ContentValues();

            values.put(DatabaseInfo.PersonTable.FIRST_NAME, this.first_name);
            values.put(DatabaseInfo.PersonTable.LAST_NAME, this.last_name);
            values.put(DatabaseInfo.PersonTable.STUDENT_NUMBER, this.student_number);
            values.put(DatabaseInfo.PersonTable.ECTS, this.ects);
            values.put(DatabaseInfo.PersonTable.PASSWORD, password);

            int id = db.insert(DatabaseInfo.PersonTable.TABLE, null, values);
            if (id != -1) {
                this.id = id;
            }
        } else {
            saveAsThread();
        }
    }

    public void setLogin(boolean login) {
        DatabaseHelper db = DatabaseHelper.getInstance();
        ContentValues values = new ContentValues();

        values.put(DatabaseInfo.PersonTable.FIRST_NAME, this.first_name);
        values.put(DatabaseInfo.PersonTable.LAST_NAME, this.last_name);
        values.put(DatabaseInfo.PersonTable.STUDENT_NUMBER, this.student_number);
        values.put(DatabaseInfo.PersonTable.LOGGED_IN, login);
        db.update(DatabaseInfo.PersonTable.TABLE, values, "_id = " + this.id);
    }

    public void update() {
        DatabaseHelper db = DatabaseHelper.getInstance();
        ContentValues values = new ContentValues();

        values.put(DatabaseInfo.PersonTable.FIRST_NAME, this.first_name);
        values.put(DatabaseInfo.PersonTable.LAST_NAME, this.last_name);
        values.put(DatabaseInfo.PersonTable.STUDENT_NUMBER, this.student_number);
        db.update(DatabaseInfo.PersonTable.TABLE, values, "_id = " + this.id);
    }

    /**
     * Deletes the object from DB
     *
     * @return boolean
     */
    public boolean delete() {
        DatabaseHelper db = DatabaseHelper.getInstance();
        return db.remove(DatabaseInfo.PersonTable.TABLE, "_id = " + this.id);
    }

    /**
     * Queries the database and returning a list of objects
     * (if where is null returns all the objects in an ArrayList)
     *
     * @param where String
     * @return ArrayList
     */
    static public ArrayList where(String where) {
        DatabaseHelper db = DatabaseHelper.getInstance();
        ArrayList<DOPerson> list = new ArrayList<DOPerson>();
        Cursor cursor = db.query(DatabaseInfo.PersonTable.TABLE, new String[]{"*"}, where, null, null, null, null);
        while (cursor.moveToNext()) {
            int id = (int) cursor.getInt(cursor.getColumnIndex("_id"));
            String first_name = (String) cursor.getString(cursor.getColumnIndex(DatabaseInfo.PersonTable.FIRST_NAME));
            String last_name = (String) cursor.getString(cursor.getColumnIndex(DatabaseInfo.PersonTable.LAST_NAME));
            String student_number = (String) cursor.getString(cursor.getColumnIndex(DatabaseInfo.PersonTable.STUDENT_NUMBER));

            DOPerson person = new DOPerson(first_name, last_name, student_number);
            person.setId(id);

            list.add(person);
        }
        cursor.close();

        return list;
    }

    /**
     * This method returns all the entries in the list.
     *
     * @return ArrayList
     */
    static public ArrayList where() {
        return where(null);
    }

    /**
     * Queries the database and returns a the first object that matches the where
     * (if where is null picks the first object in the database)
     *
     * @param where String
     * @return Module
     */
    static public DOPerson find(String where) {
        DatabaseHelper db = DatabaseHelper.getInstance();
        Cursor cursor = db.query(DatabaseInfo.PersonTable.TABLE, new String[]{"*"}, where, null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            int id = (int) cursor.getInt(cursor.getColumnIndex("_id"));
            String first_name = (String) cursor.getString(cursor.getColumnIndex(DatabaseInfo.PersonTable.FIRST_NAME));
            String last_name = (String) cursor.getString(cursor.getColumnIndex(DatabaseInfo.PersonTable.LAST_NAME));
            String student_number = (String) cursor.getString(cursor.getColumnIndex(DatabaseInfo.PersonTable.STUDENT_NUMBER));

            DOPerson person = new DOPerson(first_name, last_name, student_number);
            person.setId(id);

            cursor.close();

            return person;
        }
        return null;
    }

    static public boolean checkPassword(Context ctx, String student_number, String password) {
        DatabaseHelper db = DatabaseHelper.getInstance();
        try {
            Cursor cursor = db.query(DatabaseInfo.PersonTable.TABLE, new String[]{"*"}, "student_number = '" + student_number + "'", null, null, null, null);
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                String pass = (String) cursor.getString(cursor.getColumnIndex(DatabaseInfo.PersonTable.PASSWORD));
                if (pass.equals(password)) {
                    return true;
                } else {

                    Toast t = Toast.makeText(ctx, "Wachtwoord incorrect", Toast.LENGTH_SHORT);
                    t.show();
                }
            } else {
                Toast t = Toast.makeText(ctx, "Gebruikersnaam incorrect", Toast.LENGTH_SHORT);
                t.show();
            }
        } catch (SQLiteException ex) {
            Toast t = Toast.makeText(ctx, "Gebruikersnaam incorrect", Toast.LENGTH_SHORT);
            t.show();
        }
        return false;
    }

    public boolean checkCore(){
        if (getCore().size() < 2 ){
            return false;
        }
        return true;
    }

    public ArrayList<DOGrade> getCore(){
        ArrayList<DOCourse> courses = DOCourse.where("core = 1");
        ArrayList<DOGrade> grades = new ArrayList<DOGrade>();
        for( DOCourse c : courses){
            try {
                DOGrade grade = DOGrade.find("uid = " + this.id + " AND course_id = " + c.getId());
                if (grade.getGrade() > 5.5 || grade.isDone()) {
                    grades.add(grade);
                }
            } catch (SQLiteException ex){
                ex.printStackTrace();
            }
        }
        return grades;
    }

    public void calcPassedAndFailedGrades(){
        int[] ects = new int[] {0,0};
        ArrayList<DOGrade> grades = DOGrade.where("uid = " + this.id);
        for( DOGrade c : grades) {
            DOCourse course = DOCourse.find("_id = " + c.getCourseId() );
            if (c.getGrade() > 5.5 || c.isDone()) {
                ects[0] += course.getEcts();
            } else {
                ects[1] += course.getEcts();
            }
        }

        this.passedEcts = ects[0];
        this.failedEcts = ects[1];
    }

    public int getPassedEcts(){ return this.passedEcts; }
    public int getFailedEcts(){ return this.failedEcts; }

    public String getAdvice(){
        String advice = "Geen advies voer wat cijfers in!";

        if (this.failedEcts < this.passedEcts) {
            advice = "Je bent goed bezig!";
        }
        if (this.failedEcts > 10){
            advice = "Je kan je jaar niet meer halen, volgend jaar hard werken aan je propedeuse vakken";
        }
        if (this.failedEcts > 20){
            advice = "Je kan je jaar niet meer halen en krijgt een negatief BSA.";
        }
        if (this.passedEcts == 60){
            advice = "Je hebt je propedeuse binnen!";
        }

        return advice;
    }

    public int getCurrentPeriod(){
        Calendar calendar = new GregorianCalendar();
        Date currentTime = new Date();
        calendar.setTime(currentTime);
        int cur_week = calendar.get(Calendar.WEEK_OF_YEAR);
        if(cur_week >= 34 && cur_week <= 46){
            return 1;
        } else if(cur_week >= 47 || cur_week <= 5){
            return 2;
        } else if(cur_week >= 6 && cur_week <= 16){
            return 3;
        } else if(cur_week >= 17 && cur_week <= 28){
            return 4;
        }
        return 0;
    }
}
