package hsleiden.studiebarometer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import hsleiden.studiebarometer.Models.DOCourse;
import hsleiden.studiebarometer.Models.DOGrade;
import hsleiden.studiebarometer.Models.DOPerson;
import hsleiden.studiebarometer.Utils.OnSwipeTouchListener;

public class CourseDetailActivity extends AppCompatActivity {
    DOCourse course;
    DOPerson person;
    DOGrade grade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vak_detail);

        this.person = (DOPerson) getIntent().getSerializableExtra("person");
        this.course = (DOCourse) getIntent().getSerializableExtra("course");
        this.grade = (DOGrade) DOGrade.find("uid = " + this.person.getId() + " AND course_id = " + this.course.getId());

        TextView course_title = (TextView) findViewById(R.id.course_title);
        TextView course_ects = (TextView) findViewById(R.id.course_ects);
        TextView course_core = (TextView) findViewById(R.id.course_core);
        final EditText course_grade = (EditText) findViewById(R.id.course_grade);
        Button course_save_button = (Button) findViewById(R.id.course_save_buton);
        final CheckBox course_done = (CheckBox) findViewById(R.id.course_done);
        if (this.grade != null) {
            if (this.grade.getGrade() != 0) {
                course_grade.setText("" + this.grade.getGrade());
            }

            if (this.grade.isDone()) {
                course_done.setChecked(true);
            }
        }

        course_title.setText(this.course.getName());
        course_ects.setText("Waarde: " + this.course.getEcts() + " ECTS");
        course_core.setText("Kernvak: " + this.course.isCoreString());

        course_save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DOGrade save_grade;
                double grade_text;
                if(!course_grade.getText().toString().equals("")) {
                    grade_text = Double.parseDouble(course_grade.getText().toString());
                } else {
                    grade_text = 0;
                }

                if (grade != null){
                    save_grade = grade;
                    save_grade.setGrade(grade_text);
                } else {
                    save_grade = new DOGrade(course.getId(), person.getId(),grade_text);
                }

                if (course_done.isChecked()){
                    save_grade.setDone(true);
                } else {
                    save_grade.setDone(false);
                }

                save_grade.save();

                Intent intent = new Intent();
                setResult(RESULT_OK,intent);
                finish();
            }
        });

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.course_detail_layout);

        layout.setOnTouchListener(new OnSwipeTouchListener(CourseDetailActivity.this) {
            @Override
            public void onSwipeRight() {
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);

        return true;
    }

    //Link menu items to activities
    public boolean myOnClick(MenuItem item){
        switch(item.getItemId()) {
            case R.id.menu_home:
                Intent intent = new Intent();
                setResult(5,intent);
                finish();
                break;
            case R.id.menu_progress:
                Intent intent2 = new Intent();
                setResult(6, intent2);
                finish();
                break;
            case R.id.menu_courses:
                finish();
                break;
        }

        return true;
    }
}
