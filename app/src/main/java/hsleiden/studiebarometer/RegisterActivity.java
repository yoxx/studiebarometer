package hsleiden.studiebarometer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import hsleiden.studiebarometer.Models.DOPerson;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final EditText first_name_input = (EditText) findViewById(R.id.first_name_input);
        final EditText last_name_input = (EditText) findViewById(R.id.last_name_input);
        final EditText student_nr_input = (EditText) findViewById(R.id.student_nr_input);
        final EditText password_input = (EditText) findViewById(R.id.password_input);
        Button register_button = (Button) findViewById(R.id.register_button);

        password_input.setImeActionLabel("Login", KeyEvent.KEYCODE_ENTER);
        password_input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                        if (!first_name_input.getText().toString().equals("") || !last_name_input.getText().toString().equals("") || !student_nr_input.getText().toString().equals("") || !password_input.getText().toString().equals("")){
                            DOPerson person = new DOPerson(first_name_input.getText().toString(),last_name_input.getText().toString(),student_nr_input.getText().toString());
                            person.saveWithPassword(password_input.getText().toString());

                            Intent intent = new Intent();
                            intent.putExtra("person", person);
                            setResult(RESULT_OK, intent);
                            finish();
                        } else {
                            Toast.makeText(getBaseContext(),"Fill in all the fields!", Toast.LENGTH_SHORT);
                        }
                    }
                }
                return false;
            }
        });

        register_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!first_name_input.getText().toString().equals("") && !last_name_input.getText().toString().equals("") && !student_nr_input.getText().toString().equals("") && !password_input.getText().toString().equals("")){
                    DOPerson person = new DOPerson(first_name_input.getText().toString(),last_name_input.getText().toString(),student_nr_input.getText().toString());
                    person.saveWithPassword(password_input.getText().toString());
                    Intent intent = new Intent();
                    intent.putExtra("person", person);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    Toast.makeText(getBaseContext(),"Fill in all the fields!", Toast.LENGTH_SHORT);
                }
            }
        });
    }
}
