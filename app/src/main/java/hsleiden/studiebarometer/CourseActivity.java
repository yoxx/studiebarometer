package hsleiden.studiebarometer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import hsleiden.studiebarometer.Models.DOCourse;
import hsleiden.studiebarometer.Models.DOPerson;
import hsleiden.studiebarometer.Utils.OnSwipeTouchListener;

public class CourseActivity extends AppCompatActivity {
    private ArrayList<DOCourse> courses;
    private ArrayAdapter adapter;
    private ListView courseList;
    private DOPerson person;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vakken);
        this.courses = DOCourse.where();
        this.courseList= (ListView) findViewById(R.id.listView);

        this.person = (DOPerson) getIntent().getSerializableExtra("person");

        for(DOCourse c : courses){
            c.setUID(this.person.getId());
        }

        this.adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, courses);
        this.courseList.setAdapter(adapter);

        this.courseList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                DOCourse course = (DOCourse) courses.get(position);
                Intent intent = new Intent(CourseActivity.this, CourseDetailActivity.class);
                intent.putExtra("course", course);
                intent.putExtra("person", person);

                startActivityForResult(intent,1);
            }
        });

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.courses_layout);

        this.courseList.setOnTouchListener(new OnSwipeTouchListener(CourseActivity.this) {
            @Override
            public void onSwipeRight() {
                Intent intent = new Intent();
                setResult(5, intent);
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                this.adapter.notifyDataSetChanged();
            }
            if (resultCode == 5) {
                Intent intent = new Intent();
                setResult(5, intent);
                finish();
            }
            if (resultCode == 6) {
                Intent intent2 = new Intent();
                setResult(8, intent2);
                finish();;
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(5, intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);

        return true;
    }

    //Link menu items to activities
    public boolean myOnClick(MenuItem item){
        switch(item.getItemId()) {
            case R.id.menu_home:
                Intent intent = new Intent();
                setResult(5, intent);
                finish();
                break;
            case R.id.menu_progress:
                Intent intent2 = new Intent();
                setResult(8, intent2);
                finish();
                break;
            case R.id.menu_courses:
                break;
        }

        return true;
    }

}
