package hsleiden.studiebarometer.Database;

/**
 * Created by Yoxx on 29/03/16.
 */
public class DatabaseInfo {

    public class CourseTable {
        public static final String TABLE = "courses";
        public static final String COURSE_NAME = "course_name";
        public static final String ECTS = "ects";
        public static final String PERIOD = "period";
        public static final String CORE = "core";
    }

    public class GradeTable {
        public static final String TABLE = "grades";
        public static final String UID = "uid";
        public static final String COURSE_ID = "course_id";
        public static final String GRADE = "grade";
        public static final String CHANCE = "chance";
        public static final String DONE = "done";
    }

    public class PersonTable {
        public static final String TABLE = "persons";
        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String STUDENT_NUMBER = "student_number";
        public static final String PASSWORD = "password";
        public static final String LOGGED_IN = "logged_in";
        public static final String ECTS = "ects";
    }
}
