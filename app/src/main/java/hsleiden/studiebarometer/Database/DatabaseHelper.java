package hsleiden.studiebarometer.Database;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hsleiden.studiebarometer.HTTP.GsonRequest;
import hsleiden.studiebarometer.HTTP.VolleyHelper;
import hsleiden.studiebarometer.Models.DOCourse;

/**
 * Created by Yoxx on 29/03/16.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public static SQLiteDatabase mSQLDB;
    private static DatabaseHelper mInstance;
    public static final String dbName = "studieBarometer.db";
    public static final int dbVersion = 1;

    public DatabaseHelper(Context ctx) {
        super(ctx, dbName, null, dbVersion);
    }

    public static synchronized DatabaseHelper getHelper (Context ctx){
        if (mInstance == null){
            mInstance = new DatabaseHelper(ctx);
            mSQLDB = mInstance.getWritableDatabase();
        }
        return mInstance;
    }

    public static synchronized DatabaseHelper getInstance(){
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DatabaseInfo.CourseTable.TABLE + " (" +
                BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DatabaseInfo.CourseTable.COURSE_NAME + " TEXT," +
                DatabaseInfo.CourseTable.ECTS + " INTEGER," +
                DatabaseInfo.CourseTable.PERIOD + " INTEGER," +
                DatabaseInfo.CourseTable.CORE + " INTEGER);"
        );

        db.execSQL("CREATE TABLE " + DatabaseInfo.GradeTable.TABLE + " (" +
                BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DatabaseInfo.GradeTable.COURSE_ID + " INTEGER," +
                DatabaseInfo.GradeTable.UID + " INTEGER," +
                DatabaseInfo.GradeTable.GRADE + " REAL," +
                DatabaseInfo.GradeTable.CHANCE + " INTEGER," +
                DatabaseInfo.GradeTable.DONE + " INTEGER);"
        );

        db.execSQL("CREATE TABLE " + DatabaseInfo.PersonTable.TABLE + " (" +
                BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DatabaseInfo.PersonTable.FIRST_NAME + " TEXT," +
                DatabaseInfo.PersonTable.LAST_NAME + " TEXT," +
                DatabaseInfo.PersonTable.STUDENT_NUMBER + " TEXT," +
                DatabaseInfo.PersonTable.LOGGED_IN + " INTEGER," +
                DatabaseInfo.PersonTable.ECTS + " INTEGER," +
                DatabaseInfo.PersonTable.PASSWORD + " TEXT);"
        );
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ DatabaseInfo.CourseTable.TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+ DatabaseInfo.GradeTable.TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+ DatabaseInfo.PersonTable.TABLE);
        onCreate(db);
    }

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version ){
        super(context, name, factory, version);
    }

    public int insert(String table, String nullColumnHack, ContentValues values){
        long id = mSQLDB.insert(table, nullColumnHack, values);

        return (int)id;
    }

    public boolean update(String table, ContentValues values, String where){
        return mSQLDB.update(table, values, where, null) > 0 ;
    }

    public boolean remove(String table, String where){
        return mSQLDB.delete(table, where, null) > 0;
    }

    public Cursor query(String table, String[] columns, String selection, String[] selectArgs, String groupBy, String having, String orderBy){
        return mSQLDB.query(table, columns, selection, selectArgs, groupBy, having, orderBy);
    }

    public void seedCourses(final Context ctx) {
        if (DOCourse.where().size() == 0) {
            Type type = new TypeToken<List<DOCourse>>(){}.getType();
            GsonRequest<List<DOCourse>> request = new GsonRequest<List<DOCourse>>(
                    "http://fuujokan.nl/subject_lijst.json", type, null,
                    new Response.Listener<List<DOCourse>>() {
                        @Override
                        public void onResponse(List<DOCourse> response) {
                            for (DOCourse course : response) {
                                if (course.getName().equals("IOPR1") || course.getName().equals("IOPR2") || course.getName().equals("INET") || course.getName().equals("IRDB") ){
                                    course.setCore(true);
                                }

                                course.save();
                            }
                        }
                    }, new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError error){
                    Toast t = Toast.makeText(ctx,"No internet available",Toast.LENGTH_SHORT);
                    t.show();
                }
            }
            );
            VolleyHelper.getInstance(ctx).addToRequestQueue(request);
        }
    }
}
