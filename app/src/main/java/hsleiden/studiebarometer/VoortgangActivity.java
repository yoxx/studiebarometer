package hsleiden.studiebarometer;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;

import java.util.ArrayList;

import hsleiden.studiebarometer.Models.DOPerson;
import hsleiden.studiebarometer.Utils.OnSwipeTouchListener;

public class VoortgangActivity extends AppCompatActivity {
    DOPerson person;
    PieChart mChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress);

        TextView total = (TextView) findViewById(R.id.textViewTotal);
        TextView completed = (TextView) findViewById(R.id.textViewCompletedResult);
        TextView missed = (TextView) findViewById(R.id.textViewMissedResult);
        Button toevoegen = (Button) findViewById(R.id.addButton);
        this.person = (DOPerson) getIntent().getSerializableExtra("person");

        completed.append(" " + person.getPassedEcts() + " ECTS");
        missed.append(" " + person.getFailedEcts() + " ECTS");



        toevoegen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VoortgangActivity.this, CourseActivity.class);
                intent.putExtra("person", person);
                startActivityForResult(intent, 1);
            }
        });

        mChart = (PieChart) findViewById(R.id.chart);
        mChart.setDescription("");
        mChart.setTouchEnabled(false);
        mChart.setDrawSliceText(true);
        mChart.getLegend().setEnabled(false);
        mChart.setTransparentCircleColor(Color.rgb(130, 130, 130));
        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        mChart.setCenterText("Studie Voortgang");
        mChart.setCenterTextSize(18);

        setData(person.getPassedEcts(), person.getFailedEcts());

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.progress_layout);

        layout.setOnTouchListener(new OnSwipeTouchListener(VoortgangActivity.this) {
            @Override
            public void onSwipeLeft() {
                Intent intent = new Intent();
                setResult(6, intent);
                finish();
            }
        });
    }

    private void setData(int aantal_behaald, int aantal_gefaald) {
        ArrayList<Entry> yValues = new ArrayList<>();
        ArrayList<String> xValues = new ArrayList<>();

        yValues.add(new Entry(aantal_behaald, 0));
        xValues.add("Behaalde ECTS");

        yValues.add(new Entry(aantal_gefaald, 1));
        xValues.add("Niet gehaalde ECTS");

        yValues.add(new Entry(60 - aantal_behaald, 2));
        xValues.add("Resterende ECTS");

        //  http://www.materialui.co/colors
        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(Color.rgb(67,160,71));
        colors.add(Color.rgb(235,0,0));
        colors.add(Color.rgb(253,216,53));

        PieDataSet dataSet = new PieDataSet(yValues, "ECTS");
        dataSet.setColors(colors);

        PieData data = new PieData(xValues, dataSet);
        mChart.setData(data); // bind dataset aan chart.
        mChart.invalidate();  // Aanroepen van een redraw
        Log.d("aantal =", ""+aantal_behaald);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(6, intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);

        return true;
    }

    //Link menu items to activities
    public boolean myOnClick(MenuItem item){
        switch(item.getItemId()) {
            case R.id.menu_home:
                Intent intent = new Intent();
                setResult(6, intent);
                finish();
                break;
            case R.id.menu_progress:
                break;
            case R.id.menu_courses:
                Intent intent3 = new Intent();
                setResult(7, intent3);
                finish();
                break;
        }

        return true;
    }
}
