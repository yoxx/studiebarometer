package hsleiden.studiebarometer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import hsleiden.studiebarometer.Models.DOPerson;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ImageView logo = (ImageView) findViewById(R.id.loginLogo);
        Button loginButton = (Button) findViewById(R.id.login_button);
        Button registerButton = (Button) findViewById(R.id.register_button);
        final EditText student_nr = (EditText) findViewById(R.id.login_text);
        final EditText password = (EditText) findViewById(R.id.password_text);
        logo.setImageResource(R.mipmap.hslogo);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<DOPerson> list = DOPerson.where();
                if(DOPerson.checkPassword(getBaseContext(), student_nr.getText().toString(),
                        password.getText().toString())){
                    Intent intent = new Intent();
                    intent.putExtra("person", DOPerson.find("student_number = '" + student_nr.getText().toString() + "'"));
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });

        password.setImeActionLabel("Login", KeyEvent.KEYCODE_ENTER);
        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                        if(DOPerson.checkPassword(getBaseContext(), student_nr.getText().toString(),
                                password.getText().toString())){
                            DOPerson person = DOPerson.find("student_number = '" + student_nr.getText().toString()+ "'");
                            person.setLogin(true);
                            Intent intent = new Intent();
                            intent.putExtra("person", person);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivityForResult(intent,1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK){
                setResult(RESULT_OK, data);
                finish();
            }
        }
    }
}
