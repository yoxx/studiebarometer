package hsleiden.studiebarometer;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import hsleiden.studiebarometer.Database.DatabaseHelper;
import hsleiden.studiebarometer.Models.DOPerson;
import hsleiden.studiebarometer.Utils.OnSwipeTouchListener;

public class MainActivity extends AppCompatActivity {
    DOPerson person;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Context ctx = this.getBaseContext();
        DatabaseHelper db = DatabaseHelper.getHelper(ctx);
        db.seedCourses(ctx);
        if(DOPerson.where().size() > 0 ) {
            try {
                this.person = DOPerson.find("logged_in = 1");
            } catch (SQLiteException ex) {
                ex.printStackTrace();
            }
        }
        if ( this.person == null){
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivityForResult(intent,1);
        } else {
            ImageView logo = (ImageView) findViewById(R.id.HSLogoimageView);
            TextView stuname = (TextView) findViewById(R.id.stunameTextView);
            TextView period = (TextView) findViewById(R.id.period);
            TextView total = (TextView) findViewById(R.id.totalECTS);
            TextView advice = (TextView) findViewById(R.id.advies);
            Button voortgang = (Button) findViewById(R.id.voortgangButton);
            Button vakken = (Button) findViewById(R.id.vakkenButton);

            this.person.calcPassedAndFailedGrades();
            logo.setImageResource(R.mipmap.hslogo);
            period.setText("Periode: " + this.person.getCurrentPeriod());
            total.setText(this.person.getPassedEcts() + "/60");
            stuname.setText("Hello, " + this.person.getFirstName() + "!");
            advice.setText(this.person.getAdvice());

            voortgang.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, VoortgangActivity.class);
                    intent.putExtra("person", person);
                    startActivityForResult(intent, 1);
                }
            });
            vakken.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, CourseActivity.class);
                    intent.putExtra("person", person);
                    startActivityForResult(intent, 1);
                }
            });

            RelativeLayout layout = (RelativeLayout) findViewById(R.id.main_layout);

            layout.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this) {
                @Override
                public void onSwipeRight() {
                    Intent intent = new Intent(MainActivity.this, VoortgangActivity.class);
                    intent.putExtra("person", person);
                    startActivityForResult(intent, 1);
                }

                @Override
                public void onSwipeLeft() {
                    Intent intent = new Intent(MainActivity.this, CourseActivity.class);
                    intent.putExtra("person", person);
                    startActivityForResult(intent, 1);
                }
            });
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                this.person = (DOPerson) data.getSerializableExtra("person");
                this.person.calcPassedAndFailedGrades();

                ImageView logo = (ImageView) findViewById(R.id.HSLogoimageView);
                TextView stuname = (TextView) findViewById(R.id.stunameTextView);
                TextView period = (TextView) findViewById(R.id.period);
                TextView total = (TextView) findViewById(R.id.totalECTS);
                TextView advice = (TextView) findViewById(R.id.advies);
                Button voortgang = (Button) findViewById(R.id.voortgangButton);
                Button vakken = (Button) findViewById(R.id.vakkenButton);

                this.person.calcPassedAndFailedGrades();
                logo.setImageResource(R.mipmap.hslogo);
                period.setText("Periode: " + this.person.getCurrentPeriod());
                total.setText("Gehaalde ECTS: "+this.person.getPassedEcts() + "/60");
                stuname.setText("Hallo, " + this.person.getFirstName() + "!");
                advice.setText(this.person.getAdvice());
                voortgang.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, VoortgangActivity.class);
                        intent.putExtra("person", person);
                        startActivityForResult(intent, 1);
                    }
                });
                vakken.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, CourseActivity.class);
                        intent.putExtra("person", person);
                        startActivityForResult(intent, 1);
                    }
                });
            } else if (resultCode == 5) {
                this.person.calcPassedAndFailedGrades();
                TextView total = (TextView) findViewById(R.id.totalECTS);
                TextView advice = (TextView) findViewById(R.id.advies);
                total.setText("Gehaalde ECTS: "+this.person.getPassedEcts() + "/60");
                advice.setText(this.person.getAdvice());
            } else if (resultCode == 6){

            } else if (resultCode == 7){
                Intent intent2 = new Intent(MainActivity.this, CourseActivity.class);
                intent2.putExtra("person", person);
                startActivityForResult(intent2, 1);
            }
            else if (resultCode == 8){
                Intent intent = new Intent(MainActivity.this, VoortgangActivity.class);
                intent.putExtra("person", person);
                startActivityForResult(intent, 1);
            }
            else {
                finish();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);

        return true;
    }

    //Link menu items to activities
    public boolean myOnClick(MenuItem item){
        switch(item.getItemId()) {
            case R.id.menu_home:
                break;
            case R.id.menu_progress:
                Intent intent = new Intent(MainActivity.this, VoortgangActivity.class);
                intent.putExtra("person", person);
                startActivityForResult(intent, 1);
                break;
            case R.id.menu_courses:
                Intent intent2 = new Intent(MainActivity.this, CourseActivity.class);
                intent2.putExtra("person", person);
                startActivityForResult(intent2, 1);
                break;
        }

        return true;
    }

}
